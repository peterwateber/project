import {
    UploadsModel
} from './models/UploadsModel';

export var UploadsDB = {
    saveUpload: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                uploaded_by: '',
                resource: ''
            };
        }
        var upload = new UploadsModel(data);
        upload.save(function(err, obj) {
            if (!err) {
                callback(obj);
            }
        });
    },
    getUpload: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                uploaded_by: ''
            };
            var upload = new UploadsModel(data);
            upload.findById(data.uploaded_by, function(err, obj) {
                if (!err) {
                    callback(obj);
                }
            });
        }
    }
}
