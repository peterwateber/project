var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var uploadsSchema = new Schema({
    uploaded_by: String,
    resource: String,
    date_uploaded: String
});

export var UploadsModel = mongoose.model('uploads', uploadsSchema);
