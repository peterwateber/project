var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: String,
    password: String,
    salt: String
});

export var UserModel = mongoose.model('users', userSchema);
