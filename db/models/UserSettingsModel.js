var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSettingsSchema = new Schema({
    uid: String,
    name: String,
    quote: String,
    profile_pic: String,
    updated_date: String
});

export var UserSettingsModel = mongoose.model('user_settings', userSettingsSchema);
