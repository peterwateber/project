import {
    UserSettingsModel
} from './models/UserSettingsModel';

export var UserSettingsDB = {
    save: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                uid: '',
                name: '',
                quote: '',
                profile_pic: '',
                updated_date: ''
            };
        } else {
            data.quote = '';
            data.profile_pic = '';
            data.updated_date = '';
        }
        var us = new UserSettingsModel(data);
        us.save(function(err, obj) {
            if (!err) {
                callback(obj);
            }
        });
    },
    get: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                uid: ''
            }
        }
        UserSettingsModel.find(data, function(err, result) {
            if (!err) {
                var user = result[0];
                callback({
                    profile_pic: user.profile_pic ? JSON.parse(user.profile_pic) : '',
                    name: user.name,
                    quote: user.quote,
                    updated_date: user.updated_date
                })
            }
        });
    },
    update: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                uid: '',
                name: '',
                quote: '',
                profile_pic: '',
                updated_date: ''
            };
        } else {
            data.updated_date = new Date();
        }
        UserSettingsModel.update({
            uid: data.uid
        }, {
            $set: data
        }, function(err, obj) {
            if (!err) {
                callback(obj);
            }
        });
    }
};
