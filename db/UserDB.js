import {
    UserModel
} from './models/UserModel';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    UploadsDB
} from './UploadsDB';
import {
    UserSettingsDB
} from './UserSettingsDB';



export var UserDB = {
    update: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                id: '',
                password: '',
                salt: '' //add more fields if possible
            };
        }
        UserModel.update(data, function(err, obj) {
            if (!err) {
                callback(obj);
            } else {
                callack(false);
            }
        });
    },
    checkPassword: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                id: '',
                password: ''
            };
        }
        var self = this;
        UserModel.findById(data.id, function(err, obj) {
            if (!err) {
                if (obj) {
                    var user = {
                        email: obj.email,
                        password: StringUtils.password(data.password, obj.salt)
                    }
                    self.Login(user, callback);
                } else {
                    callback(false);
                }
            }
        });
    },
    getInfo: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                id: ''
            };
        }
        UserModel.findById(data.id, function(err, obj) {
            if (!err) {
                if (obj) {
                    var user = {
                        email: obj.email
                    }
                    UserSettingsDB.get({
                        uid: data.id
                    }, function(settings) {
                        Object.assign(user, settings);
                        callback(user);
                    });
                } else {
                    callback(false);
                }
            }
        });
    },
    findUserByEmail: function(data, callback) {
        // COMPLETED
        if (typeof data !== 'object') {
            data = {
                email: ''
            };
        }
        UserModel.find({
            email: data.email
        }, function(err, obj) {
            if (!err) {
                var exists = obj.length > 0,
                    result = exists ? {
                        data: {
                            email: obj[0].email,
                            salt: obj[0].salt
                        },
                        exists: exists
                    } : {
                        exists: exists
                    };
                callback(result);
            }
        });
    },
    register: function(data, callback) {
        //COMPLETED
        if (typeof data !== 'object') {
            data = {
                email: '',
                password: '',
                salt: ''
            };
        }

        var u = new UserModel(data);
        u.save(function(err, userObj) {
            if (!err) {
                callback(userObj);
            }
        });
    },
    getUserInfoByEmail: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                email: ''
            };
        }
        UserModel.find({
            email: data.email
        }, function(err, obj) {
            if (!err) {
                callback(obj);
            }
        });
    },
    Login: function(data, callback) {
        if (typeof data !== 'object') {
            data = {
                email: '',
                password: ''
            };
        }
        UserModel.find({
            email: data.email,
            password: data.password
        }, function(err, obj) {
            var _data = {};
            if (!err) {
                if (obj.length === 1) {
                    callback({
                        _id: obj[0]._id,
                        name: obj[0].name,
                        email: obj[0].email
                    });
                } else {
                    callback(false);
                }
            }
        });
    }
}
