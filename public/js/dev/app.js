$('[data-tab]').each(function() {

    var $ele = $('[data-tab]'),
        $links = $(this).find('a'),
        $active = $($ele.find('>li.active>a').data('target'));

    $active.show();

    var history = typeof window.history !== 'undefined' ? window.history : {};

    $(this).on('click', 'a', function(e) {

        $ele.parent().find('.tab-panel').hide();
        $ele.find('>li').removeClass('active');

        var url = $(this).attr('href'),
            $active = $(this);

        $content = $($(this).attr('data-target'));
        $active.parent().addClass('active');

        $content.show();

        document.title = $(this).data('title');

        history.pushState({}, '', url);

        // Prevent the anchor's default click action
        e.preventDefault();
    });
});
