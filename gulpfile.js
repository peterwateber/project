var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    webpack = require('webpack-stream'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css');

var files = {
    css: 'public/css/dev/**/*',
    javascript: 'public/js/dev/**/*'
};

//main
gulp.task('react-webpack', function() {
    return gulp.src('views/components/**.js')
        .pipe(webpack({
            watch: true,
            entry: {
                app: './views/render.js',
                home: './views/home_render.js',
                account: './views/account_render.js',
            },
            output: {
                filename: '[name].build.js'
            },
            module: {
                loaders: [{
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                }]
            },
            node: {
                fs: 'empty'
            }
        }))
        .pipe(gulp.dest('public/js/dev/dist/'));
});

gulp.task('nodemon', function() {
    nodemon({
        script: './server.js',
        exec: 'babel-node',
        ext: 'js',
        env: {
            'NODE_ENV': 'development'
        },
        ignore: [
            'node_modules/',
            'db/'
        ],
    }).on('restart', function() {
        console.log('Restaring...');
    });
});

gulp.task('js-all', function() {
    return gulp.src('public/js/dev/*.js')
        .pipe(uglify())
        .pipe(concat('core.js'))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('js-dist', function() {
    return gulp.src('public/js/dev/dist/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/js/'));
});

gulp.task('css-all', function() {
    return gulp.src('public/css/dev/*.css')
        .pipe(cleanCSS({
            processImport: false
        }))
        .pipe(concat('core.css'))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('css-dist', function() {
    return gulp.src('public/css/dev/dist/*.css')
        .pipe(cleanCSS({
            processImport: false
        }))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('default', [
    'watch',
    'react-webpack',
    'nodemon',
    'js-all',
    'js-dist',
    'css-all',
    'css-dist'
]);

gulp.task('watch', function () {
    gulp.watch(files.javascript, ['js-all', 'js-dist']);
    gulp.watch(files.css, ['css-all', 'css-dist']);
    gulp.watch('gulpfile.js', ['default']);
});
