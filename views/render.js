import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/header';
import Menu from './components/menu';

ReactDOM.render(<Header />, document.getElementById('header'));
ReactDOM.render(<Menu />, document.getElementById('menu'));
