import React from 'react';
import classNames from 'classnames';

export default class Account extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        var tabs = (
                <div>
                    <div className='tab-panel' id='login'></div>
                    <div className='tab-panel' id='register'></div>
                    <div className='tab-panel' id='forgot'></div>
                </div>
            ),
            settings_active = classNames({
                active: this.props.active === 'settings'
            }),
            security_active = classNames({
                active: this.props.active === 'security'
            }),
            login_active = classNames({
                active: this.props.active === 'login'
            }),
            register_active = classNames({
                active: this.props.active === 'register'
            }),
            forgot_active = classNames({
                active: this.props.active === 'forgot'
            }),
            list = (
                <ul className='nav-tab' data-tab='tab'>
                    <li className={login_active}>
                        <a href='/account/login' data-target='#login' data-title={this.props.pageTitle.login}>
                            <h4>Login</h4>
                        </a>
                    </li>
                    <li className={register_active}>
                        <a href='/account/register' data-target='#register' data-title={this.props.pageTitle.register}>
                            <h4>Register</h4>
                        </a>
                    </li>
                    <li className={forgot_active}>
                        <a href='/account/forgot' data-target='#forgot' data-title={this.props.pageTitle.forgot}>
                            <h4>Forgot</h4>
                        </a>
                    </li>
                </ul>
            );

        if (this.props.is_settings) {
            tabs = (
                <div className='tab-panel' id='settings'></div>
            );
            list = (
                <ul className='nav-tab' data-tab='tab'>
                    <li className={settings_active}>
                        <a href='/account/settings' data-target='#settings' data-title={this.props.pageTitle.settings}>
                            <h4>Settings</h4>
                        </a>
                    </li>
                    <li className={security_active}>
                        <a href='/account/security' data-target='#security' data-title={this.props.pageTitle.security}>
                            <h4>Security</h4>
                        </a>
                    </li>
                </ul>
            );
        }

        return (
            <html>
                <head>
                    <title>{this.props.title}</title>
                    <link rel='stylesheet' href='/css/core.css'/>
                    <link rel='stylesheet' href='/css/font-awesome.min.css' />
                </head>
                <body>
                    <div id='wrapper'>
                        <div id='header'></div>
                        <section id='menu'></section>
                        <div id='home'>
                            <div id='card'>
                                <article className='post min settings'>
                                    <div className='tabs'>
                                        {list}
                                        {tabs}
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <script src='/js/jquery.min.js'></script>
                    <script src='/js/skel.min.js'></script>
                    <script src='/js/app.build.js'></script>
                    <script src='/js/account.build.js'></script>
                    <script src='/js/core.js'></script>
                </body>
            </html>
        );
    }
}
