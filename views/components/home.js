import React from 'react';
import classNames from 'classnames';

module.exports = class Home extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div id='main'>
                <article className='post'>
                    <header>
                        <div className='title'>
                            <h2>
                                <a href='#'>Title Heading</a>
                            </h2>
                            <p>Paragraph here...</p>
                        </div>
                        <div className='meta'>
                            <time className='published' dateTime='2015-11-01'>Dec 21, 2012</time>
                            <a href='#' className='author'>
                                <span className='name'>John Doe</span>
                                <img src='http://placehold.it/45x45'/>
                            </a>
                        </div>
                    </header>
                    <a href='#' className='image featured'>
                        <img src='http://placehold.it/900x375'/>
                    </a>
                    <p>Paragraph here...</p>
                    <footer>
                        <ul className='actions'>
                            <li>
                                <a href='#' className='button big'>
                                    Continue Reading
                                </a>
                            </li>
                        </ul>
                        <ul className='stats'>
                            <li>
                                <a href='#'>General</a>
                            </li>
                            <li>
                                <a href='#' className='icon fa-heart'>1</a>
                            </li>
                            <li>
                                <a href='#' className='icon fa-comment'>2</a>
                            </li>
                        </ul>
                    </footer>
                </article>
            </div>
        );
    }
}
