import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import {ConstUtils} from '../../../utils/ConstUtils';
import {Token} from '../token';
import classNames from 'classnames';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            URL: UrlsUtils.register,
            changed: {},
            successMessage: '',
            formData: {
                pass: ''
            },
            error: {
                name: '',
                email: '',
                pass: '',
                password: ''
            },
            frmCtrlClassNames: {
                name: '',
                email: '',
                pass: '',
                password: ''
            }
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePassChange = this.handlePassChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.fetch = this.fetch.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }
    fetch(formData) {
        var self = this;
        clearTimeout(self.state.changed);
        self.state.changed = setTimeout(function() {
            $.post(self.state.URL, formData, null, 'json').done(function(res) {
                for (var prop in res.error) {
                    if (res.error[prop]) {
                        self.state.error[prop] = res.error[prop];
                        self.state.frmCtrlClassNames[prop] = false;
                    } else {
                        self.state.error[prop] = '';
                        self.state.frmCtrlClassNames[prop] = true;
                    }
                }
                if (res.run) {
                    for (var prop in self.state.frmCtrlClassNames) {
                        self.state.frmCtrlClassNames[prop] = '';
                    }
                    $('form').trigger('reset');
                    self.state.successMessage = res.message;
                } else {
                    self.state.successMessage = null;
                }
                self.setState(self.state);
            });
        }.bind(this), 500);
    }
    handleNameChange(e) {
        this.fetch({
            type: ConstUtils.REQUEST_TYPE.Register.NAME,
            name: e.target.value
        });
    }
    handleEmailChange(e) {
        this.fetch({
            type: ConstUtils.REQUEST_TYPE.Register.EMAIL,
            email: e.target.value
        });
    }
    handlePassChange(e) {
        var self = this,
            obj = self.state;
        obj.formData.pass = e.target.value;
        self.setState(obj);
        self.fetch({
            type: ConstUtils.REQUEST_TYPE.Register.PASS,
            pass: e.target.value,
            password: self.state.formData.password
        });
    }
    handlePasswordChange(e) {
        var self = this,
            obj = self.state;
        obj.formData.password = e.target.value;
        self.setState(obj);
        self.fetch({
            type: ConstUtils.REQUEST_TYPE.Register.PASSWORD,
            pass: self.state.formData.pass,
            password: obj.formData.password
        });
    }
    formSubmit(e) {
        e.preventDefault();
        var frmData = $(e.target).serializeArray();
        frmData.push({name: 'type', value: ConstUtils.REQUEST_TYPE.Register.SUBMIT});
        this.fetch(frmData);
    }
    render() {

        var frmCtrlClassNames = this.state.frmCtrlClassNames,
            frmCtrl = {},
            message = classNames({
                'message': this.state.successMessage
                    ? true
                    : false
            });

        for (var props in frmCtrlClassNames) {
            frmCtrl[props] = classNames({
                'fg': true,
                'success': frmCtrlClassNames[props],
                'error': frmCtrlClassNames[props] === false
            });
        }
        
        return (
            <form method='post' onSubmit={this.formSubmit}>
                <div className={message}>{this.state.successMessage}</div>
                <Token token={this.props.token} />
                <div className={frmCtrl.name}>
                    <div className='addon'>
                        <i className='fa fa-user'></i>
                        <input type='text' name='name' onChange={this.handleNameChange} className='form-control' placeholder='Full Name'/>
                        <div className='alert'>{this.state.error.name}</div>
                    </div>
                </div>
                <div className={frmCtrl.email}>
                    <div className='addon'>
                        <i className='fa fa-envelope'></i>
                        <input type='text' name='email' onChange={this.handleEmailChange} className='form-control' placeholder='Email'/>
                        <div className='alert'>{this.state.error.email}</div>
                    </div>
                </div>
                <div className={frmCtrl.pass}>
                    <div className='addon'>
                        <i className='fa fa-lock'></i>
                        <input type='password' name='pass' onChange={this.handlePassChange} className='form-control' placeholder='Password'/>
                        <div className='alert'>{this.state.error.pass}</div>
                    </div>
                </div>
                <div className={frmCtrl.password}>
                    <div className='addon'>
                        <i className='fa fa-lock'></i>
                        <input type='password' name='password' onChange={this.handlePasswordChange} className='form-control' placeholder='Confirm Password'/>
                        <div className='alert'>{this.state.error.password}</div>
                    </div>
                </div>
                <input type='submit' className='button' value='Register'/>
            </form>
        );
    }
}

export default Register;
