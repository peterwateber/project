import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import classNames from 'classnames';
import {Token} from '../token';

export default class Forgot extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {

        return (
            <form method='post'>
                <div className='fg'>
                    <div className='addon'>
                        <i className='fa fa-envelope'></i>
                        <input type='text' name='email' className='form-control' placeholder='Email'/>
                    </div>
                </div>
                <input type='submit' className='button' value='Send request password'/>
            </form>
        );
    }
}
