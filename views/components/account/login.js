import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import classNames from 'classnames';
import Account from '../../account/index';
import {Token} from '../token';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            changed: {},
            message: null,
            type: 'GET',
            frmCtrlClassNames: {
                email: null,
                pass: null
            },
            error: {
                email: null,
                pass: null
            }
        };

        this.fetch = this.fetch.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePassChange = this.handlePassChange.bind(this);
    }
    fetch(formData) {
        var self = this;
        clearTimeout(self.state.changed);
        self.state.changed = setTimeout(function() {
            $.ajax({url: UrlsUtils.login, type: self.state.type, data: formData}).done(function(result) {
                self.state.error = {
                    email: null,
                    pass: null
                };
                if (typeof result.error !== 'undefined') {
                    var obj = self.state.error;
                    for (var props in result.error) {
                        if (result.error[props]) {
                            obj[props] = result.error[props];
                            self.state.frmCtrlClassNames[props] = false;
                        } else {
                            obj[props] = null;
                            self.state.frmCtrlClassNames[props] = true;
                        }
                    }
                    self.setState(obj);
                }
                if (typeof result.message !== 'undefined') {
                    self.state.message = result.message;
                    self.setState(self.state);
                }
                if (typeof result.success !== 'undefined') {
                    window.location.reload();
                }
            });
        }.bind(this), 500);
    }
    onSubmit(e) {
        e.preventDefault();
        this.state.type = 'POST';
        this.setState(this.state);
        this.fetch($(e.target).serialize());
    }
    handleEmailChange(e) {
        this.fetch({email: e.target.value, type: 'email'});
    }
    handlePassChange(e) {
        this.fetch({pass: e.target.value, type: 'pass'});
    }
    render() {
        var frmCtrlClassNames = this.state.frmCtrlClassNames,
            frmCtrl = {},
            message = classNames({
                'message': this.state.message
                    ? true
                    : false
            });

        for (var props in frmCtrlClassNames) {
            frmCtrl[props] = classNames({
                'fg': true,
                'success': frmCtrlClassNames[props],
                'error': frmCtrlClassNames[props] === false
            });
        }
        return (
            <form method='get' onSubmit={this.onSubmit}>
                <div className={message}>{this.state.message}</div>
                <Token token={this.props.token}/>
                <div className={frmCtrl.email}>
                    <div className='addon'>
                        <i className='fa fa-envelope'></i>
                        <input type='text' name='email' onChange={this.handleEmailChange} placeholder='Email' className='form-control'/>
                        <div className='alert'>{this.state.error.email}</div>
                    </div>
                </div>
                <div className={frmCtrl.pass}>
                    <div className='addon'>
                        <i className='fa fa-lock'></i>
                        <input type='password' name='pass' onChange={this.handlePassChange} placeholder='Password' className='form-control'/>
                        <div className='alert'>{this.state.error.pass}</div>
                    </div>
                </div>
                <div className='fg'>
                    <input type='checkbox' name='rem' id='rem'/>
                    <label htmlFor='rem'>Remember me</label>
                </div>
                <div className='fg'>
                    <input type='submit' value='Login' className='button'/>
                </div>
            </form>
        );
    }
}
