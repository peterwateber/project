import React from 'react';
import classNames from 'classnames';

module.exports = class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <header id='header'>
                <h1>
                    <a href='#'>Project</a>
                </h1>
                <nav className='main'>
                    <ul>
                        <li className='search'>
                            <a href='#search' className='fa-search'>Search</a>
                            <form id='search' method='get'>
                                <input type='text' name='query' placeholder='Search' />
                            </form>
                        </li>
                        <li className='menu'>
                            <a href='#menu' className='fa-bars'>Menu</a>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}
