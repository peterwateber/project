import React from 'react';

export class Token extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (<input type='hidden' name='token' value={this.props.token} />)
    }
}
