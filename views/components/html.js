import React from 'react';
import classNames from 'classnames';

module.exports = class Html extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        var styles = [];
        this.props.styles.forEach(function(s) {
            styles.push(React.createElement('link', {
                rel: 'stylesheet',
                href: s,
                key: s
            }));
        });
        return (
            <html>
                <head>
                    <title>{this.props.title}</title>
                    <link rel='stylesheet' href='/css/core.css'/>
                    {styles}
                    <script src='/js/jquery.min.js'></script>
                    <script src='/js/skel.min.js'></script>
                </head>
                <body>
                    <div id='wrapper'>
                        <div id='header'></div>
                        <section id='menu'></section>
                        <div id='main'></div>
                    </div>
                    <script src='/js/app.build.js'></script>
                    <script src='/js/home.build.js'></script>
                    <script src='/js/core.js'></script>
                </body>
            </html>
        );
    }
}
