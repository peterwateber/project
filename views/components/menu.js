import React from 'react';
import classNames from 'classnames';

module.exports = class Menu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <section>
                    <form className='search' method='get'>
                        <input type='text' name='query' placeholder='Search'/>
                    </form>
                </section>
                <section>
                    <ul className='links'>
                        <li>
                            <a href='#'>
                                <h3>Menu Here</h3>
                                <p>Paragraph here...</p>
                            </a>
                        </li>
                    </ul>
                </section>
                <section>
                    <ul className='actions vertical'>
                        <li>
                            <a href='/account' className='button big fit'>
                                Log In
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}
