import React from 'react';
import ReactDOM from 'react-dom';
import Login from './components/account/login';
import Register from './components/account/register';
import Forgot from './components/account/forgot';

ReactDOM.render(<Login />, document.getElementById('login'));
ReactDOM.render(<Register />, document.getElementById('register'));
ReactDOM.render(<Forgot />, document.getElementById('forgot'));
