import express from 'express';
import {
    Config
} from './utils/site.config';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cookieSession from 'cookie-session';
import cloudinary from 'cloudinary';
import busboy from 'connect-busboy';
import sha1 from 'node-sha1';
import reactViews from 'express-react-views';

mongoose.connect('mongodb://localhost/test');

var app = express();


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'js');
app.engine('js', reactViews.createEngine({
    beautify: true
}));

app.use(express.static('public'));
app.use(bodyParser.json());
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(busboy());
app.use(cookieParser());

app.set('trust proxy', 1);
app.use(cookieSession({
    name: 'sess',
    keys: Config.Cookie.keys
}));
app.use(function(req, res, next) {
    req.sessionOptions.maxAge = req.session.maxAge || req.sessionOptions.maxAge;
    next();
});

app.use(function(req, res, next) {
    if (!req.session.token)
        req.session.token = sha1(Date.now().toString());
    next();
});

cloudinary.config({
    cloud_name: Config.Cloudinary.cloud_name,
    api_key: Config.Cloudinary.api_key,
    api_secret: Config.Cloudinary.api_secret
});

var routes = {
    default: require('./routes/default'),
    account: require('./routes/account'),
    post: {
        account: require('./routes/post.account')
    }
};

app.use('/', routes.default);
app.use('/account', routes.account);
app.use('/account', routes.post.account);


var server = app.listen(3000, function() {
    var port = server.address().port
    console.log(port)
});
