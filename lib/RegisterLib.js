import validator from 'validator';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    SanitizersUtils
} from '../utils/SanitizersUtils';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    UserDB
} from '../db/UserDB';
import {
    UserSettingsDB
} from '../db/UserSettingsDB';
import {
    Config
} from '../utils/site.config';

export var RegisterLib = {
    name: function(value) {
        //FIXED
        if (!value) {
            return MessagesUtils.name;
        }
        var error = validator.isLength(value, 3) &&
            validator.isLength(value.replace(/\s+/g, ''), {
                min: 3,
                max: 12
            }) &&
            validator.matches(SanitizersUtils.trimString(value), /^([a-zA-Z\u00C0-\u00ff]+\s)*[a-zA-Z\u00C0-\u00ff]+$/);
        return (!error) ? MessagesUtils.name : null;
    },
    email: function(value, callback) {
        //FIXED
        if (!value) {
            return callback(MessagesUtils.email);
        }
        var valid = validator.isEmail(value);
        if (valid) {
            UserDB.findUserByEmail({
                email: value
            }, function(obj) {
                callback(obj.exists ? MessagesUtils.emailExists : null);
            });
        } else {
            callback(MessagesUtils.email);
        }
    },
    pass: function(value) {
        //FIXED
        if (!value) {
            return MessagesUtils.pass;
        }
        var error = validator.isLength(value, 6);
        return (!error) ? MessagesUtils.pass : null;
    },
    password: function(value, compare) {
        //FIXED
        if (!value) {
            return MessagesUtils.password;
        }
        var error = validator.isLength(value, 1) && validator.equals(compare, value);
        return (!error) ? MessagesUtils.password : null;
    }
}
