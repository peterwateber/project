import cloudinary from 'cloudinary';
import {
    UploadsLib
} from './UploadsLib';
import {
    SecurityLib
} from './SecurityLib';
import {
    UserUtils
} from '../utils/UserUtils';
import {
    Config
} from '../utils/site.config';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    UserDB
} from '../db/UserDB';
import {
    UploadsDB
} from '../db/UploadsDB';

var oData = {
    tab: '',
    title: 'Please Login',
    settings: false,
    pageTitles: {
        security: 'Account Security',
        settings: 'Account Settings',
        login: 'Please Login',
        register: 'Join us!',
        forgot: 'Forgot my password'
    }
};

export var AccountsLib = {
    init: function(isLogin, req, res, next) {
        if (isLogin) {
            if (req.xhr) {
                UserDB.getInfo({
                    id: req.session[Config.Sessions.name.id]
                }, function(obj) {
                    var token = {
                        token: req.session.token
                    };
                    Object.assign(obj, token);
                    res.send(obj);
                });
            } else {
                this.Settings(req, res, next);
            }
        } else {
            oData.settings = false;
            this.Login(req, res);
        }
    },
    Login: function(req, res) {
        res.render('login');
    },
    Register: function(req, res) {
        oData.tab = 'register';
        oData.title = oData.pageTitles.register;
        res.render('account/account', oData);
    },
    Security: function(req, res, next) {
        if (req.xhr) {
            var params = req.body,
                type = params.type;
            switch (type) {
                case 'check_pass':
                    SecurityLib.checkPass({
                        password: params.pass,
                        id: req.session[Config.Sessions.name.id]
                    }, function(obj) {
                        res.send({
                            error: {
                                pass: !obj ? MessagesUtils.Login.passwd : null
                            }
                        })
                    });
                    break;
                case 'validate_pass':
                    var obj = {
                        n_pass: SecurityLib.validatePass(params.n_pass)
                    };
                    if (params.c_pass) {
                        obj.c_pass = SecurityLib.validateConfirmPass(params.c_pass, params.n_pass);
                    }
                    res.send({
                        error: obj
                    });
                    break;
                case 'validate_c_pass':
                    res.send({
                        error: {
                            c_pass: SecurityLib.validateConfirmPass(params.c_pass, params.n_pass)
                        }
                    });
                    break;
                case 'change_pass':
                    SecurityLib.checkPass({
                        password: params.pass,
                        id: req.session[Config.Sessions.name.id]
                    }, function(obj) {
                        var proceed = obj && !SecurityLib.validatePass(params.n_pass) && !SecurityLib.validateConfirmPass(params.c_pass, params.n_pass);
                        if (proceed) {
                            SecurityLib.changePass({
                                id: req.session[Config.Sessions.name.id],
                                password: params.c_pass
                            }, function(obj){
                                if (obj) {
                                    UserUtils.logout(req, res, false);
                                    res.send({
                                        message: MessagesUtils.Success.Login.change_pass
                                    });
                                }
                            });
                        } else {
                            res.send({
                                error: {
                                    pass: !obj ? MessagesUtils.Login.passwd : null,
                                    n_pass: SecurityLib.validatePass(params.n_pass),
                                    c_pass: SecurityLib.validateConfirmPass(params.c_pass, params.n_pass)
                                }
                            });
                        }
                    });
                    break;
            }
        } else {
            oData.settings = true;
            oData.tab = 'security';
            oData.title = oData.pageTitles.security;
            res.render('account/account', oData);
        }
    },
    Settings: function(req, res, next) {
        if (req.xhr) {
            UploadsLib.accounts(req, res, next);
        } else {
            oData.settings = true;
            oData.tab = 'settings';
            oData.title = oData.pageTitles.settings;
            res.render('account/account', oData);
        }
    }
};
