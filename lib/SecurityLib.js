import validator from 'validator';
import {
    UserUtils
} from '../utils/UserUtils';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    Config
} from '../utils/site.config';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    UserDB
} from '../db/UserDB';

export var SecurityLib = {
    checkPass: function(data, callback) {
        UserDB.checkPassword(data, callback);
    },
    validatePass: function(value) {
        if (!value) {
            return MessagesUtils.pass;
        }
        var error = validator.isLength(value, 6);
        return (!error) ? MessagesUtils.pass : null;
    },
    validateConfirmPass: function(value, compare) {
        if (!value) {
            return MessagesUtils.password;
        }
        var error = validator.isLength(value, 1) && validator.equals(compare, value);
        return (!error) ? MessagesUtils.password : null;
    },
    changePass: function(data, callback) {
        var salt = Date.now();
        UserDB.update({
            id: data.id,
            password: StringUtils.password(data.password, salt),
            salt: salt
        }, callback);
    }
};
