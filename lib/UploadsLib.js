import cloudinary from 'cloudinary';
import {
    UserSettingsDB
} from '../db/UserSettingsDB';
import {
    UploadsDB
} from '../db/UploadsDB';
import {
    Config
} from '../utils/site.config';
import {
    SanitizersUtils
} from '../utils/SanitizersUtils';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import async from 'async';

function clean(quote) {
    if (quote) {
        return SanitizersUtils.sanitizeString(quote);
    }
    return '';
}

export var UploadsLib = {
    accounts: function(req, res, next) {
        async.parallel({
            name: function(callback) {
                req.busboy.on('field', function(fieldname, val) {
                    if (fieldname === 'name') {
                        val = StringUtils.stripString(val);
                        if (!val) {
                            callback(true, {
                                error: MessagesUtils.name
                            });
                        } else {
                            UserSettingsDB.update({
                                uid: req.session[Config.Sessions.name.id],
                                name: val
                            }, function() {
                                callback(null, val);
                            });
                        }
                    }
                });
            },
            file: function(callback) {
                req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                    var stream = cloudinary.uploader.upload_stream(function(result) {
                        var upload = {
                            id: result.public_id,
                            url: result.url,
                            format: result.format,
                            size_bytes: result.bytes
                        };

                        UserSettingsDB.update({
                            uid: req.session[Config.Sessions.name.id],
                            profile_pic: JSON.stringify(upload)
                        }, function() {
                            callback(null, upload);
                        });
                    });
                    file.pipe(stream);
                });
                //SINCE PIC BECOMES A FIELD IF FILE IS EMPTY.
                req.busboy.on('field', function(fieldname, val) {
                    if (fieldname === 'pic') {
                        callback(null, '');
                    }
                });
            },
            quote: function(callback) {
                req.busboy.on('field', function(fieldname, val) {
                    if (fieldname === 'quote') {
                        var quote = clean(val);
                        UserSettingsDB.update({
                            uid: req.session[Config.Sessions.name.id],
                            quote: quote
                        }, function() {
                            callback(null, quote)
                        });
                    }
                });
                req.pipe(req.busboy);
            }
        }, function(err, result) {
            res.send(result);
        })

    }
};
