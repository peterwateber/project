import validator from 'validator';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    UserDB
} from '../db/UserDB';
import {
    Config
} from '../utils/site.config';

export var LoginLib = {
    validateEmail: function(email) {
        var result = MessagesUtils.email;
        if (email) {
            result = !validator.isEmail(email) ? MessagesUtils.email : null;
        }
        return result;
    },
    validatePass: function(pass) {
        var result = MessagesUtils.pass;
        if (pass) {
            result = !validator.isLength(pass, 6) ? MessagesUtils.pass : null
        }
        return result;
    },
    login: function(req, res) {
        var result = {},
            validate = false,
            email = req.body.email,
            pass = req.body.pass,
            rem = req.body.rem,
            run = !this.validateEmail(email) && !this.validatePass(pass); //meaning: doesn't contain errors

        switch (req.body.type) {
            case 'email':
                validate = this.validateEmail(email);
                result.error = {
                    email: validate
                };
                break;
            case 'pass':
                validate = this.validatePass(pass);
                result.error = {
                    pass: validate
                };
                break;
            default:
                result.error = {
                    email: this.validateEmail(email),
                    pass: this.validatePass(pass)
                };
        }

        if (run) {
            UserDB.getUserInfoByEmail({
                email: email
            }, function(user) {
                if (user.length === 1) {
                    var password = StringUtils.password(pass, user[0].salt);
                    UserDB.Login({
                        email: email,
                        password: password
                    }, function(data) {
                        if (!data) {
                            res.send({
                                message: MessagesUtils.Login.invalid
                            })
                        } else {
                            req.session[Config.Sessions.name.id] = data._id;
                            if (rem) {
                                req.sessionOptions.maxAge = Config.Cookie.age;
                            }
                            res.send({
                                success: true
                            });
                        }
                    });
                    return;
                } else {
                    res.send({
                        message: MessagesUtils.Login.invalid
                    })
                }
            });
            return;
        }

        res.send(result);
    }
}
