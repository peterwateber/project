import express from 'express';
import {
    Config
} from '../utils/site.config';
import {
    ConstUtils
} from '../utils/ConstUtils';
import {
    MessagesUtils
} from '../utils/MessagesUtils';
import {
    StringUtils
} from '../utils/StringUtils';
import {
    SanitizersUtils
} from '../utils/SanitizersUtils';
import {
    RegisterLib
} from '../lib/RegisterLib';
import {
    UserDB
} from '../db/UserDB';

var router = express.Router();

router.post('/:all', function(req, res, next) {
    if (req.xhr) {
        return res.send({
            token: req.session.token
        });
    }
});

router.post('/register', function(req, res, next) {
    var params = req.body,
        result = {
            error: {}
        };
    switch (params.type) {
        case ConstUtils.REQUEST_TYPE.Register.NAME:
            result.error.name = RegisterLib.name(params.name);
            res.send(result);
            break;
        case ConstUtils.REQUEST_TYPE.Register.EMAIL:
            RegisterLib.email(params.email, function(error_message) {
                res.send({
                    error: {
                        email: error_message
                    }
                });
            });
            break;
        case ConstUtils.REQUEST_TYPE.Register.PASS:
            result.error.pass = RegisterLib.pass(params.pass);
            if (params.password) {
                result.error.password = RegisterLib.password(params.password, params.pass);
            }
            res.send(result);
            break;
        case ConstUtils.REQUEST_TYPE.Register.PASSWORD:
            result.error.password = RegisterLib.password(params.password, params.pass);
            res.send(result);
            break;
        case ConstUtils.REQUEST_TYPE.Register.SUBMIT:
            RegisterLib.email(params.email, function(error_message) {
                var result = {
                        name: RegisterLib.name(params.name),
                        email: error_message,
                        pass: RegisterLib.pass(params.pass),
                        password: RegisterLib.password(params.password, params.pass)
                    },
                    run = true;
                for (var prop in result) {
                    if (result[prop]) {
                        run = false;
                        break;
                    }
                }
                if (run) {
                    var salt = Date.now();
                    UserDB.register({
                        name: SanitizersUtils.sanitizeString(params.name),
                        email: params.email,
                        password: StringUtils.password(params.password, salt),
                        salt: salt,
                    }, function() {
                        res.send({
                            error: result,
                            run: run,
                            message: MessagesUtils.registrationSuccess
                        });
                    })
                } else {
                    res.send({
                        error: result
                    });
                }
            });
            break;
    }
});

module.exports = router;
