import express from 'express';
import {
    Config
} from '../utils/site.config';
import Render from '../import/Render';


var render = new Render(),
    router = express.Router();

function isLogin(sess) {
    return typeof sess[Config.Sessions.name.id] !== 'undefined' || sess[Config.Sessions.name.id] ? sess[Config.Sessions.name.id] : false;
}

var data = {
    pageTitle: {
        settings: 'Account Settings',
        security: 'Account Security',
        login: 'Please Login',
        register: 'Join Us!',
        forgot: 'Forgot Your Password?'
    }
};

router.get('*', function(req, res, next) {
    render.set(req, res, next);
});

router.get('/', function(req, res, next) {
    render.setData({
        active: 'login',
        title: data.pageTitle.login,
        pageTitle: data.pageTitle
    });
});

router.get('/:all', function(req, res, next) {
    switch (req.params.all) {
        case 'security':
        case 'settings':
            if (!isLogin(req.session)) {
                res.redirect('/account/login');
            } else {
                next();
            }
            break;
        default:
            if (isLogin(req.session)) {
                res.redirect('/account/');
            } else {
                next();
            }
    }
});

router.get('/login', function(req, res, next) {
    render.setData({
        active: 'login',
        title: data.pageTitle.login,
        pageTitle: data.pageTitle
    });
});

router.get('/register', function(req, res, next) {
    render.setData({
        active: 'register',
        title: data.pageTitle.register,
        pageTitle: data.pageTitle
    });
});

router.get('/forgot', function(req, res, next) {
    render.setData({
        active: 'forgot',
        title: data.pageTitle.forgot,
        pageTitle: data.pageTitle
    });
});

router.get('*', function() {
    render.toHtml();
});

module.exports = router;
