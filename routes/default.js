var express = require('express');
var router = express.Router();

var data = {
    title: 'My project',
    styles: [
        '/css/font-awesome.min.css'
    ]
}

router.get('/', function (req, res, next) {
  res.render('html', data);
});

module.exports = router;
