import express from 'express';
import {
    RegisterLib
} from '../lib/RegisterLib';
import {
    LoginLib
} from '../lib/LoginLib';
import {
    Config
} from '../utils/site.config';
import {
    AccountsLib
} from '../lib/AccountsLib';

var router = express.Router();

function isLogin(sess) {
    return typeof sess[Config.Sessions.name.id] !== 'undefined' || sess[Config.Sessions.name.id] ? sess[Config.Sessions.name.id] : false;
}

router.get('/', function(req, res, next) {
    AccountsLib.init(isLogin(req.session), req, res, next);
});

router.get('/:all', function(req, res, next) {
    switch (req.params.all) {
        case 'security':
        case 'settings':
            if (!isLogin(req.session)) {
                res.redirect('/account/login');
            } else {
                next();
            }
            break;
        default:
            if (isLogin(req.session)) {
                res.redirect('/account/');
            } else {
                next();
            }
    }
});

router.get('/login', function(req, res, next) {
    AccountsLib.Login(req, res);
});

router.post('/login', function(req, res, next){
    if (req.xhr) {
        LoginLib.login(req, res);
    }
});

router.get('/forgot', function(req, res, next) {
    oData.tab = 'forgot';
    oData.title = oData.pageTitles.forgot;
    res.render('account/account', oData);
});

router.get('/register', function(req, res, next) {
    if (req.xhr) {
        RegisterLib.run(req, res);
    } else {
        AccountsLib.Register(req, res);
    }
});

router.post('/register', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    if (req.xhr) {
        res.setHeader('Content-Type', 'application/json');
        RegisterLib.post(req, res);
    } else {
        return res.send({
            error: 'Forbidden access'
        });
    }
});

router.get('/security', function(req, res, next) {
    AccountsLib.Security(req, res, next);
});

router.post('/security', function(req, res, next) {
    AccountsLib.Security(req, res, next);
});


router.get('/settings', function(req, res, next) {
    AccountsLib.Settings(req, res, next);
});

router.post('/settings', function(req, res, next) {
    AccountsLib.Settings(req, res, next);
});

module.exports = router;
