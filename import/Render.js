export default class Render {
    constructor() {
        this.req = {};
        this.res = {};
        this.next = {};
        this.data = {
            styles: [],
            active: '',
            is_settings: false,
            token: ''
        };
    }
    set(req, res, next) {
        this.req = req;
        this.res = res;
        this.next = next;
        this.next();
    }
    setData(data) {
        data.token = this.req.session.token;
        Object.assign(this.data, data);
        this.next();
    }
    getData() {
        return this.data;
    }
    toHtml() {
        this.res.render('account/index', this.getData());
    }
}
