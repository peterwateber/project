import validator from 'validator';
import sanitizer from 'sanitizer';

export var SanitizersUtils = {
    trimString: function (str) {
        str = validator.rtrim(str);
        return validator.ltrim(str);
    },
    sanitizeString: function (str) {
        return sanitizer.unescapeEntities(this.trimString(str).replace(/\s+/g, " "));
    }
}
