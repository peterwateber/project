import {
    Config
} from './site.config';
import {
    UrlsUtils
} from './UrlsUtils';
export var UserUtils = {
    logout: function(req, res, redirect) {
        req.session[Config.Sessions.name.id] = '';
        if (redirect) {
            res.redirect(UrlsUtils.accounts)
        }
    }
}
