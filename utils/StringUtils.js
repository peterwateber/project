import sha1 from 'node-sha1';
import {
    Config
} from './site.config.js';

export var StringUtils = {
    password: function(pass, salt) {
        return sha1(Config.Password.static_hash + pass + salt);
    }
}
