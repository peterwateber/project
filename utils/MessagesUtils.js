export var MessagesUtils = {
    name: 'Oops! name doesn\'t look cool',
    email: 'Email is not valid.',
    emailExists: 'Email is already being used.',
    pass: 'Password must be at least 6 characters.',
    password: 'Confirm Password must be the same with the password.',
    captcha: 'Please verify if you\'re not a robot.',
    registrationSuccess: 'Please check your inbox to activate your account.',
    Login: {
        invalid: 'Invalid username or password. Please try again.',
        passwd: 'Password is incorrect' //used only in settings
    },
    Success: {
        Login: {
            change_pass: 'Password successfully changed. Please re-login.'
        }
    },
    HTTP: {
        Error: {
            D_Access: 'Disallowed acccess.'
        }
    }
};
