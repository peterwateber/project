export var ConstUtils = {
    REQUEST_TYPE: {
        TOKEN: 'token_access',
        Register: {
            NAME: 'check_name',
            EMAIL: 'check_email',
            PASS: 'check_pass',
            PASSWORD: 'check_c_pass',
            SUBMIT: 'validate_registration'
        }
    }
};
