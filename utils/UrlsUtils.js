export var UrlsUtils = {
    login: '/account/login',
    register: '/account/register',
    accounts: '/account',
    security: '/account/security',
    settingsUpload: '/account/settings'
};
