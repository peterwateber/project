import React from 'react';
import ReactDOM from 'react-dom';
import Login from './accounts/login';
import Register from './accounts/register';
import Settings from './accounts/settings/settings';
import Security from './accounts/settings/security';


ReactDOM.render(<Login />, document.getElementById('login'));
ReactDOM.render(<Register />, document.getElementById('register'));
ReactDOM.render(<Security />, document.getElementById('security'));
ReactDOM.render(<Settings />, document.getElementById('settings'));
