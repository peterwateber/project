import React from 'react';
import {UrlsUtils} from '../utils/UrlsUtils';

export class Token extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: ''
        };
    }
    componentWillReceiveProps(nextProp) {
        this.setState({
            token: nextProp.token
        });
    }
    render() {
        return (<input type='hidden' name='token' value={this.state.token} />)
    }
}
