import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import classNames from 'classnames';
import SettingsUpload from './settingsuploads';
import {Token} from '../../token';

class Settings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            changed: {},
            token: '',
            message: '',
            name: '',
            quote: '',
            loader: false,
            profile_pic: '', //URL
            file: '' //Binary. The FILE OBJECT
        };

        clearTimeout(this.state.changed);
        var self = this;
        this.state.changed = setTimeout(function() {
            $.ajax({url: UrlsUtils.accounts, dataType: 'json'}).done(function(res) {
                if (!res.error) {
                    self.state.quote = res.quote;
                    self.state.profile_pic = res.profile_pic.url;
                    self.state.name = res.name;
                    self.state.token = res.token;
                    self.setState(self.state);
                }
            });
        }.bind(this), 200);

        this.loader = this.loader.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.setFile = this.setFile.bind(this);
        this.handleQuoteChange = this.handleQuoteChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
    }
    setFile(file) {
        var s = this.state;
        s.file = file;
        this.setState(s);
    }
    onSubmit(e) {
        e.preventDefault();
        var self = this;
        clearTimeout(self.state.changed);
        self.state.changed = setTimeout(function() {
            var formData = new FormData();
            formData.append('pic', self.state.file);
            formData.append('name', self.state.name);
            formData.append('quote', self.state.quote);
            formData.append('token', self.state.token);
            self.state.loader = true;
            self.setState(self.state);
            $.ajax({
                url: UrlsUtils.settingsUpload,
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false
            }).done(function(obj) {
                for (var prop in obj) {
                    if (typeof obj[prop] !== 'object') {
                        self.state.message = null;
                        self.state.file = null;
                        self.state.quote = obj.quote;
                        self.state.loader = false;
                    } else if (typeof obj[prop] === 'object' && obj[prop].error) {
                        self.state.message = obj[prop].error;
                    }
                    self.setState(self.state);
                }
            });
        }.bind(this), 200);
    }
    loader() {
        return this.state.loader;
    }
    handleQuoteChange(e) {
        var s = this.state;
        s.quote = e.target.value;
        this.setState(s);
    }
    handleNameChange(e) {
        var s = this.state;
        s.name = e.target.value;
        this.setState(s);
    }
    render() {
        var message = classNames({
            'message': this.state.message
                ? true
                : false
        });
        return (
            <div>
                <form onSubmit={this.onSubmit} encType='multipart/form-data'>
                    <h4 className='form-title'>Profile</h4>
                    <Token token={this.state.token}/>
                    <div className='fg'>
                        <SettingsUpload pic={this.state.profile_pic} showLoader={this.loader} setFile={this.setFile}/>
                    </div>
                    <div className={message}>{this.state.message}</div>
                    <div className='fg'>
                        <div className='addon'>
                            <i className='fa fa-user'></i>
                            <input type='text' value={this.state.name} onChange={this.handleNameChange} className='form-control' placeholder='Full Name'/>
                        </div>
                    </div>
                    <div className='fg'>
                        <div className='addon'>
                            <i className='fa fa-quote-left'></i>
                            <input type='text' value={this.state.quote} onChange={this.handleQuoteChange} className='form-control' placeholder='Quote...'/>
                        </div>
                    </div>
                    <div className='fg'>
                        <input type='submit' className='button' value='Save'/>
                    </div>
                </form>
            </div>
        );
    }
}

export default Settings;
