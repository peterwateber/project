import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import classNames from 'classnames';
import {Token} from '../../token';

class Security extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            changed: {},
            token: '',
            message: '',
            formData: {
                type: '',
                pass: '',
                n_pass: '',
                c_pass: ''
            },
            formDataStatus: {
                pass: '',
                n_pass: '',
                c_pass: ''
            },
            error: {
                pass: '',
                n_pass: '',
                c_pass: ''
            }
        };

        var self = this;
        clearTimeout(this.state.changed);
        this.state.changed = setTimeout(function() {
            $.ajax({url: UrlsUtils.accounts, dataType: 'json'}).done(function(res) {
                if (!res.error) {
                    self.state.token = res.token;
                    self.setState(self.state);
                }
            });
        }.bind(this), 200);

        this.onSubmit = this.onSubmit.bind(this);
        this.checkPassword = this.checkPassword.bind(this);
        this.checkNewPassword = this.checkNewPassword.bind(this);
        this.checkChangePassword = this.checkChangePassword.bind(this);
        this.submit = this.submit.bind(this);
    }
    submit() {
        var self = this;
        clearTimeout(self.state.changed);
        self.state.changed = setTimeout(function() {
            var formData = self.state.formData;
            Object.assign(formData, {token: self.state.token});
            $.ajax({url: UrlsUtils.security, type: 'POST', data: formData}).done(function(res) {
                for (var prop in res.error) {
                    self.state.error[prop] = res.error[prop]
                        ? res.error[prop]
                        : null;
                    self.state.formDataStatus[prop] = res.error[prop]
                        ? false
                        : true;
                }
                self.state.message = res.message || '';
                self.setState(self.state);
            });
        }.bind(this), 200);
    }
    onSubmit(e) {
        e.preventDefault();
        this.state.formData.type = 'change_pass';
        this.setState(this.state);
        this.submit();
    }
    checkPassword(e) {
        this.state.formData.type = 'check_pass';
        this.state.formData.pass = e.target.value;
        this.setState(this.state);
        this.submit();
    }
    checkNewPassword(e) {
        this.state.formData.type = 'validate_pass';
        this.state.formData.n_pass = e.target.value;
        this.setState(this.state);
        this.submit();
    }
    checkChangePassword(e) {
        this.state.formData.type = 'validate_c_pass';
        this.state.formData.c_pass = e.target.value;
        this.setState(this.state);
        this.submit();
    }
    render() {
        var fg = {},
            message = {};
        for (var prop in this.state.formDataStatus) {
            fg[prop] = classNames({
                'fg': true,
                'success': this.state.formDataStatus[prop],
                'error': this.state.formDataStatus[prop] === false
            });
        }

        message = classNames({
            message: this.state.message
                ? true
                : false
        });

        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <h4 className='form-title'>Change Password</h4>
                    <div className={message}>{this.state.message}</div>
                    <Token token={this.state.token}/>
                    <div className={fg.pass}>
                        <div className='addon'>
                            <i className='fa fa-lock'></i>
                            <input type='password' onChange={this.checkPassword} placeholder='Old Password'/>
                            <div className='alert'>{this.state.error.pass}</div>
                        </div>
                    </div>
                    <div className={fg.n_pass}>
                        <div className='addon'>
                            <i className='fa fa-lock'></i>
                            <input type='password' onChange={this.checkNewPassword} placeholder='New Password'/>
                            <div className='alert'>{this.state.error.n_pass}</div>
                        </div>
                    </div>
                    <div className={fg.c_pass}>
                        <div className='addon'>
                            <i className='fa fa-lock'></i>
                            <input type='password' onChange={this.checkChangePassword} placeholder='Re-type Password'/>
                            <div className='alert'>{this.state.error.c_pass}</div>
                        </div>
                    </div>
                    <div className='fg'>
                        <input type='submit' className='button' value='Save'/>
                    </div>
                </form>
            </div>
        );
    }
}

export default Security;
