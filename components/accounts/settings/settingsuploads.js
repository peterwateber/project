import React from 'react';
import {UrlsUtils} from '../../../utils/UrlsUtils';
import classNames from 'classnames';

class SettingsUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pic: props.pic || ''
        };
        this.handleFile = this.handleFile.bind(this);
    }
    handleFile(e) {
        var reader = new FileReader();
        var file = e.target.files[0];
        reader.onload = function(upload) {
            var _file = upload.target.result;
            this.setState({pic: _file});
        }.bind(this);

        reader.readAsDataURL(file);
        this.props.setFile(file);
    }
    componentWillReceiveProps (prop) {
        this.state.pic = prop.pic;
    }
    render() {
        var _loader = classNames({
            loader: true,
            show: this.props.showLoader()
        });
        return (
            <div className='upload-cli'>
                <img className={_loader} src='/images/loader.gif' />
                <a href='#' className='avatar-circle'>
                    <div className='avatar-circle-caption'>
                        <i className='fa fa-camera'></i>
                        <h3>Change picture</h3>
                        <input type='file' onChange={this.handleFile}/>
                    </div>
                    <img src={this.state.pic}/>
                </a>
            </div>
        );
    }
}

export default SettingsUpload;
